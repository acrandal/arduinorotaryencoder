# ArduinoRotaryEncoder

Inexpensive rotary encoders are noisy. Each dedent point might move in either direction as you rotate if you don't debounce effectively. This library provides an example of using timers in the Arduino Uno / ATMega 328p to handle noise.